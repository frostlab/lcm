"""Setup file to install the LCM Python package."""

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

packages = find_packages(where=".")
print("PACKAGES: ", packages)

package_data = {
    '': [
        "./*.so",
        "./*.dll",
        "./*.pyd"
    ]
}

# Cleaner to read in the contents rather than copy them over.
readme_contents = open("${PROJECT_SOURCE_DIR}/README.md").read()

setup(
    name='lcm',
    version='1.4.4',
    description="Lightweight Communications and Marshalling",
    url="https://bitbucket.org/frostlab/lcm",
    long_description_content_type='text/markdown',
    license='GNU LESSER GENERAL PUBLIC LICENSE',
    long_description=readme_contents,
    # https://pypi.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'Topic :: Communications',
        'Framework :: Robot Framework'
    ],
    packages=packages,
    include_package_data=True,
    package_data=package_data,
    zip_safe=False,
)