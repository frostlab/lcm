include(CMakeParseArguments)

#------------------------------------------------------------------------------
function(lcm_concat VAR)
  foreach(_line ${ARGN})
    set(${VAR} "${${VAR}}${_line}\n")
  endforeach()
  set(${VAR} "${${VAR}}" PARENT_SCOPE)
endfunction()

#------------------------------------------------------------------------------
macro(lcm_option NAME DOCSTRING TEST)
  # Look for the dependency; if the option already exists and is ON, go ahead
  # and require it, otherwise just see if we find it
  if(DEFINED ${NAME} AND ${NAME})
    find_package(${ARGN} REQUIRED)
  else()
    find_package(${ARGN})
  endif()

  # Create the option; ON by default if the dependency is found, otherwise OFF
  if(${TEST})
    set(_${NAME}_DEFAULT ON)
  else()
    set(_${NAME}_DEFAULT OFF)
  endif()
  option(${NAME} ${DOCSTRING} ${_${NAME}_DEFAULT})
  unset(_${NAME}_DEFAULT)

  # If the option is ON and we didn't already find the dependency, require it
  # now
  if(${NAME} AND NOT ${TEST})
    find_package(${ARGN} REQUIRED)
  endif()
endmacro()

#------------------------------------------------------------------------------
function(lcm_copy_file_target TARGET INPUT OUTPUT)
  get_filename_component(DESTINATION ${OUTPUT} PATH)
  add_custom_command(
    OUTPUT ${OUTPUT}
    DEPENDS ${INPUT}
    COMMAND ${CMAKE_COMMAND} -E make_directory ${DESTINATION}
    COMMAND ${CMAKE_COMMAND} -E copy ${INPUT} ${OUTPUT}
  )

  add_custom_target(${TARGET} ALL
    DEPENDS ${OUTPUT}
  )
endfunction()

# https://stackoverflow.com/questions/13959434/cmake-out-of-source-build-python-files
function(create_symlinks source_folder dest_folder)
  if(${source_folder} STREQUAL ${dest_folder})
    return()
  endif()

  file(GLOB files
       LIST_DIRECTORIES true
       RELATIVE "${source_folder}"
       "${source_folder}/*")
  foreach(path_file ${files})
    get_filename_component(folder ${path_file} PATH)
    get_filename_component(ext ${path_file} EXT)
    set(ignored_ext ".tpl" ".h")
    list (FIND ignored_ext "${ext}" _index)
    if (${_index} GREATER -1)
      continue()
    endif ()
    # Create REAL folder
    file(MAKE_DIRECTORY "${dest_folder}")

    # Delete symlink if it exists
    file(REMOVE "${dest_folder}/${path_file}")

    # Get OS dependent path to use in `execute_process`
    file(TO_NATIVE_PATH "${dest_folder}/${path_file}" link)
    file(TO_NATIVE_PATH "${source_folder}/${path_file}" target)

    # cmake-format: off
    if(UNIX)
      set(command ln -s ${target} ${link})
    else()
      set(command cmd.exe /c mklink ${link} ${target})
    endif()
    # cmake-format: on

    execute_process(COMMAND ${command}
                    RESULT_VARIABLE result
                    ERROR_VARIABLE output)

    if(NOT ${result} EQUAL 0)
      message(
        FATAL_ERROR
          "Could not create symbolic link for: ${target} --> ${output}")
    endif()

  endforeach(path_file)
endfunction(create_symlinks)