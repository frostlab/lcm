FROM gcc:11

RUN apt-get update
RUN apt-get install -qy cmake libglib2.0-dev build-essential python3-dev python3-pip